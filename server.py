from bottle import run, Bottle, static_file, request
from gevent import monkey; monkey.patch_all()
import json

import smtplib

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

html = open('email.html', 'r').read()
emailTo = 'blackmius@gmail.com'

def sendEmail(name, email, message):
	msg = MIMEMultipart('alternative')
	msg['Subject'] = 'Portfolio contact request from ' + name
	msg['From'] = 'portfolio@emailer'
	msg['To'] = emailTo
	text = MIMEText(html.format(name=name, email=email, message=message), 'html')
	msg.attach(text)

	s = smtplib.SMTP_SSL('smtp.yandex.ru', 465)
	s.login('ysgo@yandex.ru', 'aa02062009')
	s.sendmail('ysgo@yandex.ru', emailTo, msg.as_string())

app = Bottle()

@app.get('/')
def index():
	return static_file('index.html', root='src')

@app.get('/<filename:path>')
def static(filename):
	return static_file(filename, root='src')

@app.post('/form')
def send_form():
	data = json.loads(request.body.read().decode('utf-8'))
	name = data['name'].strip()
	email = data['email'].strip().lower()
	message = data['message'].strip()
	if not (email and name and message):
		return '{"message":"Not all data given."}'
	sendEmail(name, email, message)
	return '{"success":true,"message":"Your application accepted successfully."}'

run(app, host='127.0.0.1', port='8005', server='gevent')
