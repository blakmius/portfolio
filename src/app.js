require([
	'lib/body',
	'lib/smoothscroll',
	'lib/reqwest'
], (z, smoothscroll, reqwest) => {

const breakpoints = {
	xlarge: '(max-width: 1800px)',
	large: '(max-width: 1280px)',
	medium: '(max-width: 980px)',
	notSmall: '(min-width: 769px)',
	small: '(max-width: 769px)',
	xsmall: '(max-width: 480px)'
}

const breakpoint = (a, b) => `@media screen and ${breakpoints[a]} {${b}}`;

const Style = z('style', `
*, *:before, *:after { box-sizing: border-box; }
body { font-family: 'Noto Sans', sans-serif; margin: 0; overflow: hidden; }

.serif { font-family: 'Noto Serif', serif; }

.c0 { color: #272727; }
.c1 { color: #aaa; }
.c2 { color: #fff; }
.c3 { color: #888; }
.c4 { color: #787878; }
.c5 { color: #a2a2a2; }

.error { color: #e29292; }
.success { color: #92e2a8; }

.f0 { font-size: 12px; line-height: 24px; }
.f05 { font-size: 15px; line-height: 28px; }
.f1 { font-size: 16px; line-height: 28px; }
.f2 { font-size: 20px; line-height: 35px; }
.f3 { font-size: 28px; line-height: 42px; }
.f4 { font-size: 32px; line-height: 48px; }

.w6 { font-weight: 600; }

.sp1 { margin-top: 15px; }
.sp2 { margin-top: 30px; }
.sp3 { margin-top: 45px; }
.sp4 { margin-top: 60px; }

.text { white-space: pre-wrap; word-wrap: break-word; }

.h0, .h1 { transition: color .2s, border-bottom-color .2s; cursor: pointer; border-bottom: dotted 1px; }
.h0:hover, .h1:hover { border-bottom-color: transparent; }
.h0:hover { color: #eee; } .h1:hover { color: #49bf9d; }

.flex { display: flex; }

.flex.dir-col { flex-direction: column; }
.flex.dir-row { flex-direction: row; }
.flex.dir-row-r { flex-direction: row-reverse; }

.flex.align-end { align-items: flex-end; }
.flex.align-center { align-items: center; }

.flex.wrap { flex-wrap: wrap; }
.flex.nowrap { flex-wrap: nowrap; }

.flex.content-spbetween { justify-content: space-between; }
.flex.content-end { justify-content: flex-end; }

.flex.list > * { margin: 0.75rem; }
.flex.list > *:last-child { margin-right: 0; }

.fl0 { flex: auto!important; }
.fl1 { flex: 1!important; }
.fl2 { flex: 2!important; }
.fl3 { flex: 3!important; }
.fl4 { flex: 4!important; }
.fl5 { flex: 5!important; }
.fl6 { flex: 6!important; }

.sz0 { width: 1em; }

.bg0 { background-image: linear-gradient(rgba(0, 0, 0, 0.15), rgba(0, 0, 0, 0.5)), url('assets/images/overlay.png'),
	url('assets/images/bg.jpg'); background-repeat: no-repeat, repeat, no-repeat; background-size: cover; }

.tr { text-align: right; }
.tc { text-align: center; }
.tdn { text-decoration: none!important; }

.p0 { padding: 6em 3em 3em 3em; }

.nosel { user-select: none; -moz-user-select: none; }

u { text-decoration: none; border-style: solid; border-width: 0 0 1px 0;
  padding: 0.2rem 0; margin: -0.2rem 0 calc(0.2rem - 1px) 0; }

input, textarea { color: #a2a2a2; outline: none; padding: 0 12px; border: 2px solid transparent; width: 100%;
	border-radius: 2px; }
input::placeholder, textarea::placeholder { color: #a2a2a2; }
input:focus, textarea:focus { border-color: #49bf9d; }
input { height: 3em; }
textarea { min-height: 120px; }

button { border-radius: 2px; transition: all .2s; border: 2px solid #efefef;
	padding: 12px 16px; background: none; outline: none; cursor: pointer; }
button:hover { color: #49bf9d; border-color: #49bf9d; }

.scrollY { overflow-y: scroll; }

.avatar { border-radius: 100%; width: 6.25em; }
.icon { border-bottom: 0; }
.icon:before { font-family: FontAwesome; }

hr { border: 0; border-top: solid 2px #efefef; }

.preview { width: 100%; border-radius: 2px; }

.overlay { width: 100vw; height: 100vh; }

.column { display: block; flex-basis: 0; flex-grow: 1;
	flex-shrink: 1; padding: 0.75rem; }

.columns { margin: -0.75rem -0.75rem 0.75rem -0.75rem; }

a { text-decoration: none; color: inherit; }

${breakpoint('small', `
	.p0 { padding: 6em 2em 3em 2em; }
`)}

${breakpoint('medium', `
	.overlay { flex-direction: column; height: auto; }
	body { overflow-y: auto; }
	.header { text-align: center; align-items: center!important; }
	.icons { justify-content: center!important; }
	#main { overflow: hidden; }
`)}

${breakpoint('notSmall', `
	.columns { display: flex; }
`)}

`);

const Icon = name => z(`.icon.c3.f2.fa-${name}`);
const LinkIcon = (icon, href) => z({is: `a.icon.h0.c3.f3.tdn.fa-${icon}`, href});

const Header = z('.header.flex.fl2.bg0.c1.p0.dir-col.align-end.content-spbetween.tr',
	z('.v',
		z('<', '<img src="assets/images/avatar.jpg" class="avatar">'),
		z('.f2.sp1.text',
			'Hello, \nI\'m ', z('span.c2.w6', 'Daniel'), '\nA ',
			z('span.c2.w6', 'FullStack'), ' web developer'
		)
	),
	z('.v',
		z('.icons.flex.dir-row.list.content-end',
			LinkIcon('telegram', 'https://t.me/Blackmius'),
			LinkIcon('github', 'https://github.com/blackmius'),
			LinkIcon('phone', 'tel:89182011931'),
			LinkIcon('envelope-o', 'mailto:blackmius@gmail.comnk'),
		),
		z('p.f0',
			'Designed and developed by ',
			z('span.c2.w6',  'Blackmius')
		)
	)
);

const About = z('',
	z('.f3.c4', 'About me'),
	z('.f05.sp2.c5.text', `I'm a full-stack web developer with 5 years of professional experience.
The scope of my work is a large part of the front end: HTML/CSS/JS,
building Single Page Apps with Zombular, but many times
I get my hands dirty with some back end applications written in Python.

I'm available for remote work - if you would like to build something together, get in `,
	z({is: 'span.h1', onclick: e => (window.innerWidth <= 769) ? smoothscroll('ContactMe', 500)
			: smoothscroll('ContactMe', 500, 'main')}, 'touch'), '?')
);

const projects = [
	{name: 'Scratch CTF 2016', description: 'CTF competition platform', preview: 'ScratchCTF.png', href: 'assets/images/projects/ScratchCTF.png'},
	{name: 'Cherrigram', description: 'Service for promoting a instagram accounts', preview: 'Cherrigram.png', href: 'assets/images/projects/Cherrigram.png'},
	{name: 'Bedlam', description: 'Accounting of arrivals at a sanatorium', preview: 'Bedlam.png', href: 'assets/images/projects/Bedlam.png'},
	{name: 'Sochi IT School', description: 'Landing page', preview: 'SochiITSchool.png', href: 'assets/images/projects/SochiITSchool.png'},
	{name: 'QFix Pro', description: 'Landing page', preview: 'QFixPro.png', href: 'http://qfix.pro'},
	{name: 'Roza Vetrov', description: 'Landing page', preview: 'RozaVetrov.png', href: 'assets/images/projects/RozaVetrovFull.png'},
	{name: 'Spbkuznec catalog', description: 'Catalog page', preview: 'Spbkuznec.png', href: 'http://spbkuznec.ru/catalog/#fences'}
]

const Project = (name, description, preview, href) => z('.sp2.column',
	z('<', `<a href="${href}" target="_blank"><img class="preview" src="assets/images/projects/${preview}"></a>`),
	z('.f1.c4', name),
	z('.f0.c5', description)
);

let projects_ = [], columns = 2;
while (projects.length > 0) projects_.push(z('.columns',
	projects.splice(0, columns).map(i=>Project(i.name, i.description, i.preview, i.href))
));

const Works = z('.sp3',
	z('.f3.c4', 'Recent Work'),
	projects_
);

const Input = (label, val) => z({is: 'input.f1', oninput: e => {
	val.set(e.target.value); z.update(); }, spellcheck: 'false', placeholder: label, value: val.get});

const TextBox = (label, val) => z({is: 'textarea.f1', oninput: e => {
	val.set(e.target.value); z.update(); }, spellcheck: 'false', placeholder: label, value: val.get});

const Button = (text, action) => z({is: 'button.c4.f1', onclick: action}, text);

let data = {
	name: z.Val(''),
	email: z.Val(''),
	message: z.Val('')
}
let message = {};

const ContactMe = z('#ContactMe.sp3',
	z('.f3.c4', 'Contact me'),
	z('.sp3', z('.columns',
		z('.column.fl4',
			z('.columns',
				z('.column', Input('Name', data.name)),
				z('.column', Input('Email', data.email)),
			),
			z('.sp1', TextBox('Message', data.message)),
			()=>z({is: '.sp1', class: {error:!message.success, success: message.success}}, message.message),
			z('.sp1', Button('Send Message', e => {
				reqwest({
					url: '/form',
					method: 'post',
					data: JSON.stringify({
						name:data.name.get(),
						email:data.email.get(),
						message:data.message.get()
					}),
					success: res => {
						try {
							message = JSON.parse(res);
							z.update();
						} catch(e) {}
					}
				});
			}))
		),
		z('.column.fl2.c4',
			z('.flex.dir-row.nowrap.align-center.list',
				z('.sz0.tc', Icon('mobile')),
				z('.v', '8(918)-201-19-31')
			),
			z('.flex.dir-row.nowrap.align-center.list',
				z('.sz0.tc', Icon('envelope-o')),
				z({is: 'a.h1', href:'mailto:blackmius@gmail.com'}, 'blackmius@gmail.com')
			)
		)
	))
)

const Main = z('#main.fl6.p0.scrollY',
	About, z('hr.sp3'), Works,
	z('hr.sp3'), ContactMe
);

const Body = z('',
	Style,
	z('.flex.overlay',
		Header,
		Main
	)
	
);

z.setBody(Body)

});